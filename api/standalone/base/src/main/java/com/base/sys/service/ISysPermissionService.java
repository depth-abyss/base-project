package com.base.sys.service;

import com.base.sys.entity.SysPermission;
import com.baomidou.mybatisplus.extension.service.IService;
import com.base.util.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
public interface ISysPermissionService extends IService<SysPermission> {

    Result permissionList();
}
