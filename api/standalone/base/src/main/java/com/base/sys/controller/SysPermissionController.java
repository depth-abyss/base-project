package com.base.sys.controller;


import com.base.sys.service.ISysPermissionService;
import com.base.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
@RestController
@RequestMapping("/sys/sys-permission")
public class SysPermissionController {

    @Autowired
    private ISysPermissionService sysPermissionService;


    @RequestMapping("/permission-list")
    public Result permissionList(){
        return sysPermissionService.permissionList();
    }


}
