package com.base.sys.dto;

import com.base.util.PageQuery;

public class SysRoleQueryDTO extends PageQuery {

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
