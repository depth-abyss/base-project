package com.base.sys.mapper;

import com.base.sys.dto.PermissionDTO;
import com.base.sys.entity.SysPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gjj
 * @since 2018-11-27
 */
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    List<PermissionDTO> permissionList();
}
