package com.base.sys.service.impl;

import com.base.sys.entity.SysUserUpdateLog;
import com.base.sys.mapper.SysUserUpdateLogMapper;
import com.base.sys.service.ISysUserUpdateLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gjj
 * @since 2018-12-06
 */
@Service
public class SysUserUpdateLogServiceImpl extends ServiceImpl<SysUserUpdateLogMapper, SysUserUpdateLog> implements ISysUserUpdateLogService {

}
