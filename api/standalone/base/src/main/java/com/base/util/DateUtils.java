package com.base.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public DateUtils() {
    }

    public static String format(Date date) {
        return format(date, "yyyy-MM-dd");
    }

    public static String format(Date date, String pattern) {
        if (date != null) {
            SimpleDateFormat df = new SimpleDateFormat(pattern);
            return df.format(date);
        } else {
            return null;
        }
    }

    public static String getTimeBefore(Date date) {
        Date now = new Date();
        long l = now.getTime() - date.getTime();
        long day = l / 86400000L;
        long hour = l / 3600000L - day * 24L;
        long min = l / 60000L - day * 24L * 60L - hour * 60L;
        long s = l / 1000L - day * 24L * 60L * 60L - hour * 60L * 60L - min * 60L;
        String r = "";
        if (day > 0L) {
            r = r + day + "天";
        } else if (hour > 0L) {
            r = r + hour + "小时";
        } else if (min > 0L) {
            r = r + min + "分";
        } else if (s > 0L) {
            r = r + s + "秒";
        }

        r = r + "前";
        return r;
    }

    public static String getTimeBeforeAccurate(Date date) {
        Date now = new Date();
        long l = now.getTime() - date.getTime();
        long day = l / 86400000L;
        long hour = l / 3600000L - day * 24L;
        long min = l / 60000L - day * 24L * 60L - hour * 60L;
        long s = l / 1000L - day * 24L * 60L * 60L - hour * 60L * 60L - min * 60L;
        String r = "";
        if (day > 0L) {
            r = r + day + "天";
        }

        if (hour > 0L) {
            r = r + hour + "小时";
        }

        if (min > 0L) {
            r = r + min + "分";
        }

        if (s > 0L) {
            r = r + s + "秒";
        }

        r = r + "前";
        return r;
    }
}
