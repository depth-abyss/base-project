package com.base.sys.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import com.base.sys.dto.UserMenuPermissionDTO;
import com.base.util.ExcelField;
import com.base.util.ExcelSheet;
import com.base.util.ObjectField;
import org.apache.commons.beanutils.ConvertUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author gjj
 * @since 2018-11-23
 */
@TableName("SYS_USER")
@ExcelSheet("系统用户")
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    /**
     * 用户名(登录使用)
     */
    @TableField("USERNAME")
    @ExcelField("用户名")
    @ObjectField("用户名")
    private String username;

    /**
     * 真实姓名
     */
    @TableField("NAME")
    @ExcelField("姓名")
    @ObjectField("姓名")
    private String name;

    @TableField(exist = false)
    @ExcelField("启用禁用")
    private String statusValue;

    @TableField("MOBILE")
    @ExcelField("手机号")
    @ObjectField("手机号")
    private String mobile;

    @TableField("EMAIL")
    @ExcelField("邮箱")
    @ObjectField("邮箱")
    private String email;

    @TableField("GENDER")
    @ObjectField(value="性别",dicType="sex")
    private Integer gender;

    @TableField(exist = false)
    @ExcelField("性别")
    private String genderValue;

    /**
     * 密码
     */
    @TableField("PASSWORD")
    private String password;




    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    @ExcelField("创建时间")
    private Date createTime;

    /**
     * 修改时间
     */
    @TableField("UPDATE_TIME")
    @ExcelField("修改时间")
    private Date updateTime;
    /**
     * 版本
     */
    @TableField("VERSION")
    @Version
    private Integer version;


    @TableField(exist = false)
    private Boolean rememberMe;

    @TableField("AVATAR")
    private String avatar;

    @TableField("STATUS")
    private Integer status;

    @TableField(exist = false)
    private UserMenuPermissionDTO userMenuPermission = new UserMenuPermissionDTO();

    @TableField(exist = false)
    private String roles;

    @TableField(exist = false)
    private String roleIdStr;

    @TableField(exist = false)
    private String[] roleIds=new String[0];

    @TableField(exist = false)
    private String code;

    @TableField(exist = false)
    private String dealAvatar;


    public String getDealAvatar() {
        return dealAvatar;
    }

    public void setDealAvatar(String dealAvatar) {
        this.dealAvatar = dealAvatar;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getGenderValue() {
        return genderValue;
    }

    public void setGenderValue(String genderValue) {
        this.genderValue = genderValue;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }



    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public String getRoleIdStr() {
        return roleIdStr;
    }

    public void setRoleIdStr(String roleIdStr) {
        this.roleIdStr = roleIdStr;
        if(!StringUtils.isEmpty(roleIdStr)){
            String[] convert = ( String[]) ConvertUtils.convert(roleIdStr.split(","), String.class);
            setRoleIds(convert);
        }
    }
    public String[] getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(String[] roleIds) {
        this.roleIds = roleIds;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


    public UserMenuPermissionDTO getUserMenuPermission() {
        return userMenuPermission;
    }

    public void setUserMenuPermission(UserMenuPermissionDTO userMenuPermission) {
        this.userMenuPermission = userMenuPermission;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(Boolean rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }



    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "SysUser{" +
        "id=" + id +
        ", username=" + username +
        ", name=" + name +
        ", password=" + password +
        ", updateTime=" + updateTime +
        ", createTime=" + createTime +
        ", version=" + version +
        "}";
    }
}
