package com.base.shiro;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.base.exception.MyException;
import com.base.sys.dto.UserMenuPermissionDTO;
import com.base.sys.entity.SysUser;
import com.base.sys.service.ISysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

@Component
public class UserRealm extends AuthorizingRealm {

    @Autowired
    private ISysUserService iSysUserService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        System.out.println("授权----------------------------");
        SysUser webUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        UserMenuPermissionDTO userMenuPermission = iSysUserService.getUserMenuPermission(webUser.getId());
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        if(userMenuPermission==null){
            userMenuPermission=new UserMenuPermissionDTO();
        }
        authorizationInfo.addStringPermissions(userMenuPermission.getPermissionList());
        return authorizationInfo;
    }

    /**
     * 验证当前登录的Subject
     * LoginController.login()方法中执行Subject.login()时 执行此方法
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
        try {
            UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
            String name = token.getUsername();
            String password = String.valueOf(token.getPassword());
            // 从数据库获取对应用户名密码的用户
            QueryWrapper wrapper = new QueryWrapper<SysUser>();
            wrapper.eq("USERNAME", name);
            SysUser one = iSysUserService.getOne(wrapper);
            if (one == null) {
                throw new AuthenticationException();
            }
            wrapper.eq("STATUS",1);
            one = iSysUserService.getOne(wrapper);
            if (one == null) {
                throw new MyException("该账号已被禁用,请联系管理员");
            }
            String dataPassword = one.getPassword();
            password = DigestUtils.md5DigestAsHex(password.getBytes("utf-8"));
            if (!password.equals(dataPassword)) {
                throw new AuthenticationException();
            }
            SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                    one, //用户
                    one.getPassword(), //密码
                    getName()  //realm name
            );
            return authenticationInfo;
        } catch (Exception e) {
            e.printStackTrace();
            throw new AuthenticationException();
        }
    }
}