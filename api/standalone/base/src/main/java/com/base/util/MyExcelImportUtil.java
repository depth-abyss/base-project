package com.base.util;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyExcelImportUtil {

    protected static List<Object> importExcel(Class<?> sheetClass, Workbook workbook) {
        try {
            ExcelSheet excelSheet = (ExcelSheet)sheetClass.getAnnotation(ExcelSheet.class);
            String sheetName = excelSheet != null && excelSheet.value() != null && excelSheet.value().trim().length() > 0 ? excelSheet.value().trim() : sheetClass.getSimpleName();
            List<Field> fields = new ArrayList();
            int rowIndex;
            if (sheetClass.getDeclaredFields() != null && sheetClass.getDeclaredFields().length > 0) {
                Field[] var5 = sheetClass.getDeclaredFields();
                int var6 = var5.length;

                for(rowIndex = 0; rowIndex < var6; ++rowIndex) {
                    Field field = var5[rowIndex];
                    if (!Modifier.isStatic(field.getModifiers())) {
                        fields.add(field);
                    }
                }
            }

            if (fields != null && fields.size() != 0) {
                Sheet sheet = workbook.getSheetAt(0);
                Iterator<Row> sheetIterator = sheet.rowIterator();
                rowIndex = 0;

                ArrayList dataList;

                ArrayList headList=new ArrayList();

                for(dataList = new ArrayList(); sheetIterator.hasNext(); ++rowIndex) {
                    Row rowX = (Row)sheetIterator.next();
                    if(rowIndex==0){
                        Iterator<Cell> cellIterator = rowX.cellIterator();
                        while(cellIterator.hasNext()){
                            Cell cell = cellIterator.next();
                            String stringCellValue = cell.getStringCellValue();
                            if(!StringUtils.isEmpty(stringCellValue)){
                                headList.add(stringCellValue);
                            }

                        }
                    }
                    if (rowIndex > 0) {
                        Object rowObj = sheetClass.newInstance();

                        for(int i = 0; i < fields.size(); ++i) {
                            Field field = (Field)fields.get(i);
                            ExcelField excelField = field.getAnnotation(ExcelField.class);
                            if(excelField==null){
                                continue;
                            }
                            String name = excelField.value().trim();
                            if(StringUtils.isEmpty(name)){
                                continue;
                            }
                            int index = headList.indexOf(name);
                            if(index<0){
                                continue;
                            }
                            String fieldValueStr = rowX.getCell(index).getStringCellValue();
                            Object fieldValue = FieldReflectionUtil.parseValue(field, fieldValueStr);
                            field.setAccessible(true);
                            field.set(rowObj, fieldValue);
                        }

                        dataList.add(rowObj);
                    }
                }

                return dataList;
            } else {
                throw new RuntimeException(">>>>>>>>>>> xxl-excel error, data field can not be empty.");
            }
        } catch (IllegalAccessException var15) {
            throw new RuntimeException(var15);
        } catch (InstantiationException var16) {
            throw new RuntimeException(var16);
        }
    }

    protected static List<Object> importExcel(Class<?> sheetClass, InputStream inputStream) {
        try {
            Workbook workbook = WorkbookFactory.create(inputStream);
            List<Object> dataList = importExcel(sheetClass, workbook);
            return dataList;
        } catch (IOException var4) {
            throw new RuntimeException(var4);
        } catch (InvalidFormatException var5) {
            throw new RuntimeException(var5);
        }
    }

}
